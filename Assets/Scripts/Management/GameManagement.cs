﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManagement : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public GameObject gameOverUI;
    public GameObject victoryUI;
    public GameObject pauseButton;
    public GameObject ball;
    public Collider2D colliderDoor;
    public Animator openDoor;
    GameObject spawnPointB;
    GameObject spawnPointP;
    GameObject[] target;
    public Slider dashBar;
    public Image[] imgBall;
    public Text scoreBox;
    public GameObject canvas;
    public Image dashImg;
    public GameObject muteButton;
    public int numLevels;
    bool isMute;

    bool gameIsPaused;
    public static int ballCount = 6;
    public static bool destroyedBall;
    public static int countEnemy;
    public static bool door;
    public static float sizeBar;
    public static int scoreValue = 0;
    public static bool playerIsAlive = false;

    bool gameManagerExists;
    bool canvasExists;

    public GameObject player;

    void Awake()
    {
        spawnPointB = GameObject.FindWithTag ("SpawnBall");
        spawnPointP = GameObject.FindWithTag("SpawnPlayer");

        playerIsAlive = false;

        FindObjectOfType<AudioController>().musicGameplay.Play();

        destroyedBall = false;
        gameIsPaused = false;

        target = GameObject.FindGameObjectsWithTag("Enemy");  
        countEnemy = target.Length;
        door = false;

        Debug.Log("O numero de inimigos é: " + target.Length);

        dashBar.minValue = 0f;
        dashBar.maxValue = 10f;
        dashBar.value = 10f;
        sizeBar = 10f;
    }

    private void Start() 
    {
        Invoke("SpawnBall", 0);
        SpawnPlayer();
    }

    void Update()
    {
        spawnPointP = GameObject.FindWithTag("SpawnPlayer");

        PlayerPrefs.SetInt("scoreValue", scoreValue);
        PlayerPrefs.SetInt("ballCount", ballCount);

        scoreBox.text = "Pontos: " + scoreValue;

        if(Input.GetKeyDown(KeyCode.P))
        {
            Pause();
        }

        if(Input.GetKeyDown(KeyCode.M))
        {
            Mute();
        }

        #region BallCount

        if(ballCount > 0)
        {
            if(destroyedBall)
            {
                destroyedBall = false;
                ballCount--;
                Debug.Log("Bolas: " + ballCount);
                SpawnBall();
                FindObjectOfType<AudioController>().loseLife.Play();
            }
        }

        else if (ballCount == 0)
        {
            Debug.Log("Acabou as vidas");
            GameOver();
        }

        switch (ballCount)
        {
            case 1:
                imgBall[0].enabled = false;
                imgBall[1].enabled = false;
                imgBall[2].enabled = false;
                imgBall[3].enabled = false;
                imgBall[4].enabled = false;
                break;
            case 2:
                imgBall[0].enabled = true;
                imgBall[1].enabled = false;
                imgBall[2].enabled = false;
                imgBall[3].enabled = false;
                imgBall[4].enabled = false;
                break;
            case 3:
                imgBall[0].enabled = true;
                imgBall[1].enabled = true;
                imgBall[2].enabled = false;
                imgBall[3].enabled = false;
                imgBall[4].enabled = false;
                break;
            case 4:
                imgBall[0].enabled = true;
                imgBall[1].enabled = true;
                imgBall[2].enabled = true;
                imgBall[3].enabled = false;
                imgBall[4].enabled = false;
                break;
            case 5:
                imgBall[0].enabled = true;
                imgBall[1].enabled = true;
                imgBall[2].enabled = true;
                imgBall[3].enabled = true;
                imgBall[4].enabled = false;
                break;
        }
        #endregion

        if (countEnemy <= 0)
        {
            openDoor.SetBool("Open", true);
            colliderDoor.enabled = false;
            Debug.Log("Acabou os inimigos");
        }

        if (door)
        {
            NextScene();
            Invoke("NewScene", 0.1f);
            door = false;
            scoreValue = PlayerPrefs.GetInt("scoreValue");
            ballCount = PlayerPrefs.GetInt("ballCount");
        }

        DashBarControl(); 
        SpawnPlayer();
    }

    public void DashBarControl() 
    {   
        if (dashBar.value >= dashBar.maxValue)
        {
            dashBar.value = dashBar.maxValue;
            dashImg.enabled = true;
        }
        else 
        {
            dashBar.value = sizeBar;
            dashImg.enabled = false;
        }

        if (dashBar.value <= dashBar.minValue)
        {
            dashBar.value = dashBar.minValue;
        }
        else 
        {
            dashBar.value = sizeBar;
        }
    }

    void SpawnBall()
    {
        if (ballCount > 0)
        {
            if (countEnemy >= 1)
            {
                Instantiate(ball, spawnPointB.transform.position, spawnPointB.transform.rotation);

            }
        }
    }

    public void SpawnPlayer()
    {
        if (!playerIsAlive)
        {
            Instantiate(player, spawnPointP.transform.position, spawnPointP.transform.rotation);
            playerIsAlive = true;
            sizeBar = 10f;
        }
    }

    void NewScene()
    {
        sizeBar = 10f;
        colliderDoor.enabled = true;
        countEnemy = target.Length;
        SpawnPlayer();
        scoreValue = PlayerPrefs.GetInt("scoreValue");
        ballCount = PlayerPrefs.GetInt("ballCount");
    }

    #region Menu

    public void NextScene()
    {   
        if ((SceneManager.GetActiveScene().buildIndex + 1) == numLevels)
        {
            Victory();
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    public void Mute()
    {
        isMute = ! isMute;
        AudioListener.volume =  isMute ? 0 : 1;
        
        if (isMute)
            muteButton.SetActive(true);
        else
            muteButton.SetActive(false);
    }

    public void Pause ()
    {
        gameIsPaused = ! gameIsPaused;
        
        if (gameIsPaused)
        {
            pauseMenuUI.SetActive(true);
            pauseButton.SetActive(true);
            gameOverUI.SetActive(false);
            victoryUI.SetActive(false);
            Time.timeScale = 0f;
            
        }
        else
        {
            pauseMenuUI.SetActive(false);
            pauseButton.SetActive(false);
            gameOverUI.SetActive(false);
            victoryUI.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    public void GameOver ()
    {
        gameOverUI.SetActive(true);
        pauseMenuUI.SetActive(false);
        //victoryUI.SetActive(false);
        gameIsPaused = true;
        Time.timeScale = 0f;
        scoreValue = 0;
        ballCount = 6;
        FindObjectOfType<AudioController>().musicGameplay.Stop();
        FindObjectOfType<AudioController>().musicGameOver.Play();
    }

    public void Victory ()
    {
        victoryUI.SetActive(true);
        pauseMenuUI.SetActive(false);
        gameOverUI.SetActive(false);
        gameIsPaused = true;
        Time.timeScale = 0f;
        scoreValue = 0;
        ballCount = 6;
        //FindObjectOfType<AudioController>().musicGameplay.Stop();
        //FindObjectOfType<AudioController>().musicGameOver.Play();
    }

    public void Restart ()
    {
        pauseMenuUI.SetActive(false);
        gameOverUI.SetActive(false);
        victoryUI.SetActive(false);
        gameIsPaused = false;
        Time.timeScale = 1f;
        scoreValue = 0;
        ballCount = 6;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        FindObjectOfType<AudioController>().musicGameOver.Stop();
        FindObjectOfType<AudioController>().musicGameplay.Play();
    }

    public void Menu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1f;
        gameIsPaused = false;
        scoreValue = 0;
        ballCount = 6;
        FindObjectOfType<AudioController>().musicGameplay.Stop();
        FindObjectOfType<AudioController>().musicGameOver.Stop();
        FindObjectOfType<AudioController>().musicMenu.Play();
    }

    public void QuitGame ()
    {
        Debug.Log("Quit game");
        Application.Quit();
    }

    #endregion
}
