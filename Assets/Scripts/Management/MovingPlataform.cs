﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlataform : MonoBehaviour
{   public float maxX, minX, maxY, minY;
    public bool moveX = true;
    public bool moveY = false;
    
    
    float dirX, moveSpeed = 3f;
    bool moveRight = true;
    bool moveLeft = true;

    
    void Update()
    {
        if (moveX)
        {
            if (transform.position.x > maxX)
                moveRight = false;
            if (transform.position.x < minX)
                moveRight = true;

            if(moveRight)
                transform.position = new Vector2(transform.position.x + moveSpeed * Time.deltaTime, transform.position.y);
            else
                transform.position = new Vector2(transform.position.x - moveSpeed * Time.deltaTime, transform.position.y);
        }

        if (moveY)
        {
            if (transform.position.y > maxY)
                moveLeft = false;
            if (transform.position.y < minY)
                moveLeft = true;

            if(moveLeft)
                transform.position = new Vector2(transform.position.x, transform.position.y + moveSpeed * Time.deltaTime);
            else
                transform.position = new Vector2(transform.position.x, transform.position.y - moveSpeed * Time.deltaTime);
        }
    }
}
