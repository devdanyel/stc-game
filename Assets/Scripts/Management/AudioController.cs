﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{
    public AudioSource musicGameplay;
    public AudioSource musicMenu;
    public AudioSource musicGameOver;
    public AudioSource dashPlayer;
    public AudioSource jumpPlayer;
    public AudioSource hitEnemy;
    public AudioSource deathEnemy;
    public AudioSource loseLife;
    public AudioSource ballBouncing;
    public AudioMixer audioMixer;
    public static AudioController instance;

    private void Start() 
    {
        if(instance == null)
        {
           instance = this;
           DontDestroyOnLoad(gameObject);
        }else if(instance != this)
        {
           Destroy(gameObject);
        }
    }

    public void SetVolumeMaster (float volumeMaster)
    {
        audioMixer.SetFloat("masterVolume", volumeMaster);
    }

    public void SetVolumeMusic (float volumeMusic)
    {
        audioMixer.SetFloat("musicVolume", volumeMusic);
    }

    public void SetVolumeEffects (float volumeEffects)
    {
        audioMixer.SetFloat("effectsVolume", volumeEffects);
    }
}
