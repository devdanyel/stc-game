﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MenuManagement : MonoBehaviour
{
    public GameObject mainMenuUI;
    public GameObject optionsMenuUI;
    public GameObject levelMenuUI;
    public AudioController audioController;
    
    public AudioMixer audioMixer;

    Resolution[] resolutions;

    public Dropdown resolutionDropdown;

    void Start() 
    {
        FindObjectOfType<AudioController>().musicMenu.Play();

        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width &&
                resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullScreen (bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void OptionsMenu()
    {
        mainMenuUI.SetActive(false);
        levelMenuUI.SetActive(false);
        optionsMenuUI.SetActive(true);
    }

    public void LevelsMenu()
    {
        mainMenuUI.SetActive(false);
        optionsMenuUI.SetActive(false);
        levelMenuUI.SetActive(true);
    }

    public void BackMenu()
    {
        mainMenuUI.SetActive(true);
        optionsMenuUI.SetActive(false);
        levelMenuUI.SetActive(false);
    }
    
    public void StartLevel()
    {
        SceneManager.LoadScene(1);
        FindObjectOfType<AudioController>().musicMenu.Stop();
        FindObjectOfType<AudioController>().musicGameplay.Play();
    }

    public void Level1()
    {
        SceneManager.LoadScene(1);
        FindObjectOfType<AudioController>().musicMenu.Stop();
        FindObjectOfType<AudioController>().musicGameplay.Play();
    }

    public void Level2()
    {
        SceneManager.LoadScene(2);
        FindObjectOfType<AudioController>().musicMenu.Stop();
        FindObjectOfType<AudioController>().musicGameplay.Play();
    }

    public void Level3()
    {
        SceneManager.LoadScene(3);
        FindObjectOfType<AudioController>().musicMenu.Stop();
        FindObjectOfType<AudioController>().musicGameplay.Play();
    }

    public void QuitGame ()
    {
        Debug.Log("Quit game");
        Application.Quit();
    }
}
