﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : Enemy
{
    public float moveSpeed = 3f;   
    bool moveLeft = true;
    public int spiderScore;
    public int spiderLife;
    public static bool attackSpider = false;
    Animator animator;
    
    void Start()
    {
        life = spiderLife;
        enemyValue = spiderScore;
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        LifeCheck();
        AnimationController();

        if (transform.position.y > 5.5f)
            moveLeft = false;
        if (transform.position.y < 0)
            moveLeft = true;

        if(moveLeft)
            transform.position = new Vector2(transform.position.x, transform.position.y + moveSpeed * Time.deltaTime);
        else
            transform.position = new Vector2(transform.position.x, transform.position.y - moveSpeed * Time.deltaTime);
    }

    void AnimationController()
    {
        switch (life)
        {
            case 1:
                animator.SetInteger("Life", 1);
                break;
            case 2:
                animator.SetInteger("Life", 2);
                break;
            case 3:
                animator.SetInteger("Life", 3);
                break;
        }

        if (attackSpider)
        {
            animator.SetTrigger("Attack");
            Spider.attackSpider = false;
        }
    }
}
