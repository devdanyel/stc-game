﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CocoonSpider : MonoBehaviour
{
    public float moveSpeed = 2f;
    public GameObject spider;
    public float timeStun;

    void Start() 
    {
        Spider.attackSpider = true;
    }

    void Update()
    {
        if (spider == null)
        {
            PlayerMovement.stun = false;
            Destroy(this.gameObject);
        }
        
        transform.position = new Vector2(transform.position.x, transform.position.y - moveSpeed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("Casulo acertou player");
        if (col.gameObject.tag == "Player")
        {
            this.transform.position = spider.transform.position;
            Spider.attackSpider = true;
            PlayerMovement.stun = true;
            PlayerMovement.stunnedTime = timeStun;

            FindObjectOfType<AudioController>().loseLife.Play();
        }
        else
        {
            if (col.gameObject.tag == "Ground")
            {
                this.transform.position = spider.transform.position;
                Spider.attackSpider = true;
            }
        }
    }
}
