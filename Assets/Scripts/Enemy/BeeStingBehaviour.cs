﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeStingBehaviour : MonoBehaviour
{
    Vector3 target;
    Vector3 sting;
    float shootCounter;
    Vector3 startPosition;
    Vector3 newPosition;
    void Start()
    {
        shootCounter = 0f;
        startPosition = this.transform.position;
    }

    void Update()
    {
        if(shootCounter < 4f)
        {
            if (GameManagement.playerIsAlive)
            {
                if (target != null)
                {
                    //target = GameObject.FindWithTag("Player").transform.position;
                    target = GameObject.FindGameObjectWithTag("Player").transform.position;
                    target.z = 0f;
                    sting = this.gameObject.transform.position;

                    target.x -= sting.x;
                    target.y -= sting.y;

                    float angle = Mathf.Atan2(target.y,target.x)* Mathf.Rad2Deg;
                    this.transform.rotation = Quaternion.Euler(new Vector3(0,0,angle + 90f));
                }
                else
                {
                    Debug.Log("Sem target");
                }
            }
        }
        
        if(shootCounter >= 4f)
        {
            newPosition = this.gameObject.transform.position;
            newPosition += -this.gameObject.transform.up * Time.deltaTime * 1.5f;
            this.gameObject.transform.position = newPosition;
        }
        shootCounter += Time.deltaTime;
        //Debug.Log(shootCounter);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("Ferrão acertou player");
        if (col.gameObject.tag == "Player")
        {
            this.transform.position = startPosition;
            GameManagement.ballCount--;
            FindObjectOfType<AudioController>().loseLife.Play();
            shootCounter = 0f;
        }
        else
        {
            if (col.gameObject.tag == "Ground")
            {
                this.transform.position = startPosition;
                shootCounter = 0f;
            }
        }

        
    }
}
