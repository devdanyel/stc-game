﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    
    public int life;
    public int enemyValue;

    public void LifeCheck()
    {
        if (life <= 0)
        {
            Debug.Log("Enemy morreu");
            Destroy(this.gameObject);
            GameManagement.scoreValue += enemyValue;
            GameManagement.countEnemy--;
            FindObjectOfType<AudioController>().deathEnemy.Play();
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ball")
        {
            Debug.Log("Player acertou Enemy");
            life--;
            FindObjectOfType<AudioController>().hitEnemy.Play();
        }
    }
}
