﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bee : Enemy
{
    Animator animator;
    //public Transform spriteEnemy;
    public int beeScore;
    public int beeLife;

    void Start()
    {
        animator = GetComponent<Animator>();

        life = beeLife;
        enemyValue = beeScore;
    }

    void Update()
    {
        LifeCheck();
        AnimationController();
    }

    void AnimationController()
    {
        switch (life)
        {
            case 1:
                animator.SetInteger("Life", 1);
                break;
            case 2:
                animator.SetInteger("Life", 2);
                break;
            case 3:
                animator.SetInteger("Life", 3);
                break;
        }
    }
}
