﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : Enemy
{
    Animator animator;
    int batScore;
    public int batLife;

    void Start()
    {
        animator = GetComponent<Animator>();

        switch (batLife)
        {
            case 1:
                batScore = 10;
                break;
            case 2:
                batScore = 20;
                break;
            case 3:
                batScore = 30;
                break;
        }

        life = batLife;
        enemyValue = batScore;
    }

    void Update()
    {
        LifeCheck();
        AnimationController();
    }

    void AnimationController()
    {
        switch (life)
        {
            case 1:
                animator.SetInteger("Life", 1);
                break;
            case 2:
                animator.SetInteger("Life", 2);
                break;
            case 3:
                animator.SetInteger("Life", 3);
                break;
        }
    }
}
