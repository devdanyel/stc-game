﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Butterfly : Enemy
{
    //Enemy enemy = new Enemy();
    public Transform[] targetFollow;
    int index;
    public int speed = 1;
    bool randomIndex = true; 
    int c = 0;


    void Start()
    {
        life = 1;
        enemyValue = 10;
    }

    void Update()
    {
        LifeCheck();

        if (randomIndex)
        {
            index = Random.Range(0, targetFollow.Length);
            randomIndex = false;
        }
        else
        {
            if (c <= 20)
            {
                c++;
            }
            else
            {
                randomIndex = true;
                c = 0;
            }
        }
        
        transform.position = Vector2.MoveTowards(transform.position, targetFollow[index].position, speed * Time.deltaTime);
    }

}
