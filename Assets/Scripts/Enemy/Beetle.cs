﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beetle : Enemy
{
    public float moveSpeed = 3f;   
    bool moveRight = true;
    public int beetleScore;
    public int beetleLife;
    Animator animator;
    public GameObject body;
    float closedTime;
    bool closedWings;
    public float idleTime, maxX, minX;

    void Start()
    {
        life = beetleLife;
        enemyValue = beetleScore;
        animator = GetComponent<Animator>();
        closedTime = idleTime;
    }

    void Update()
    {
        LifeCheck();
        AnimationController();

        if (transform.position.x > maxX)
        {
            moveRight = false;
            closedWings = true;
        }
                
        if (transform.position.x < minX)
        {
            moveRight = true;
            closedWings = true;
        }

        if (closedTime >= 0 && closedWings)
        {
            closedTime--;
            //Debug.Log(closedTime);
        }
        else
        {
            closedWings = false;
            closedTime = idleTime;
        } 

        if(moveRight)
        {
            if (!closedWings)
            {
                transform.position = new Vector2(transform.position.x + moveSpeed * Time.deltaTime, transform.position.y);
                body.SetActive(false);
            }
            else
            {
                body.SetActive(true);
            }
        }
        else
        {
            if (!closedWings)
            {
                transform.position = new Vector2(transform.position.x - moveSpeed * Time.deltaTime, transform.position.y );
                body.SetActive(false);
            }
            else
            {
                body.SetActive(true);
            }
        }
    }

    void AnimationController()
    {
        switch (life)
        {
            case 1:
                animator.SetInteger("Life", 1);
                break;
            case 2:
                animator.SetInteger("Life", 2);
                break;
            case 3:
                animator.SetInteger("Life", 3);
                break;
            case 4:
                animator.SetInteger("Life", 4);
                break;
            case 5:
                animator.SetInteger("Life", 5);
                break;
        }

        if (closedWings)
        {
            animator.SetBool("ClosedWings", true);
        }
        else
        {
            animator.SetBool("ClosedWings", false);
        }
    }
}
