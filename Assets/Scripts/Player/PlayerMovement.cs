﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController2D controller;

    public float runSpeed = 2f;
    float horizontalMove = 0f;
    bool jump = false;
    bool doubleJumpAllowed = false;
    public static float stunnedTime;
    public static bool stun;
    public bool wallCheck;
    private Rigidbody2D rBody;
    private int direction;
    private int flagDash = 0;
    private float dashTime;
    public float dashSpeed;
    public float startDashTime;
    private float moveInput;
    public float speed;
    private SpriteRenderer playerSprite;

    public bool isSwinging;
    public Vector2 ropeHook;
    bool m_Grounded;

    Animator animator;
    public GameObject stunAnim;
    //GameObject target;

    public float swingForce = 4f;

    public void Start()
    {
        animator = GetComponent<Animator>();
        //playerMovement = GetComponent<PlayerMovement>();
        dashTime = startDashTime;

        playerSprite = GetComponent<SpriteRenderer>();

        rBody = GetComponent<Rigidbody2D>();

        //target = GameObject.FindGameObjectWithTag("Spike");
        stun = false;
        stunnedTime = 0;
    }

    void Update()
    {
        if (Time.timeScale == 1f && stun == false)
        {
            animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

            horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

            if (Input.GetButtonDown("Jump"))
            {
                jump = true;
                FindObjectOfType<AudioController>().jumpPlayer.Play();
            }

            Dash ();
        }

        if (stun)
        {
            animator.SetFloat("Speed", 0.0f);
        }
        
        var halfHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y;
        var halfWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x;

        if (horizontalMove < 0)
        {
            wallCheck = Physics2D.Raycast(new Vector2(transform.position.x - halfWidth - 0.04f, transform.position.y), Vector2.up, 0.025f);

        }
        else if (horizontalMove > 0)
        {
            wallCheck = Physics2D.Raycast(new Vector2(transform.position.x + halfWidth + 0.04f, transform.position.y), Vector2.up, 0.025f);
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Spike")
        {
            Debug.Log("Player Morreu");
            Destroy(this.gameObject);
            GameManagement.playerIsAlive = false;
            GameManagement.ballCount--;
            FindObjectOfType<AudioController>().loseLife.Play();
        }

        if (col.gameObject.tag == "Plataform")
        {
            this.transform.parent = col.transform;
        }

        // Sistema de quebrar objeto
        if (col.gameObject.CompareTag("Breakable")) {
            if (flagDash == 1) {
                Destroy(col.gameObject);
            }
        }
    } 

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Plataform")
        {
            this.transform.parent = null;
        }
    } 

    public void Dash ()
    {
        // moveInput = Input.GetAxis("Horizontal");
        flagDash = 0;
        rBody.velocity = new Vector2(horizontalMove * speed, rBody.velocity.y);

        // Sistema de dash
        if (direction == 0) {
            if (Input.GetKey ("a") && Input.GetKeyDown(KeyCode.LeftShift)) {
                direction = 1;
            } else if (Input.GetKey ("d") && Input.GetKeyDown(KeyCode.LeftShift)) {
                direction = 2;
            }  else if (Input.GetKey ("s") && Input.GetKeyDown(KeyCode.LeftShift)) {
                direction = 4;
            }
        } else {
            if (dashTime <=0) {
                direction = 0;
                dashTime = startDashTime;
                rBody.velocity = Vector2.zero;
            } 
            else {
                dashTime -= Time.deltaTime; 
                if (direction == 1) {
                    rBody.velocity = Vector2.left * dashSpeed;
                    flagDash = 1;
                } else if (direction == 2) {
                    rBody.velocity = Vector2.right * dashSpeed;
                    flagDash = 1;
                } else if (direction == 4) {
                    rBody.velocity = Vector2.down * dashSpeed;
                    flagDash = 1;
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (stunnedTime >= 0)
        {
            stun = true;
            stunnedTime--;
        }
        else
        {
            stun = false;
        }

        if (!stun)
        {
            stunAnim.SetActive(false);
            controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
            jump = false;
        }
        else
        {
            stunAnim.SetActive(true);
            controller.Move(0.0f, false, jump);
            jump = false;
        }
        
        if (wallCheck)
        {
            Debug.Log("PAREDE");

            rBody.velocity = new Vector2(rBody.velocity.x, 0);
        }


        if (horizontalMove < 0f || horizontalMove > 0f)
        {
            //animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
            playerSprite.flipX = horizontalMove < 0f;
            if (isSwinging)
            {
                //animator.SetBool("IsSwinging", true);

                // 1 - Get a normalized direction vector from the player to the hook point
                var playerToHookDirection = (ropeHook - (Vector2)transform.position).normalized;

                // 2 - Inverse the direction to get a perpendicular direction
                Vector2 perpendicularDirection;
                if (horizontalMove < 0)
                {
                    perpendicularDirection = new Vector2(-playerToHookDirection.y, playerToHookDirection.x);
                    var leftPerpPos = (Vector2)transform.position - perpendicularDirection * -2f;
                    Debug.DrawLine(transform.position, leftPerpPos, Color.green, 0f);
                }
                else
                {
                    perpendicularDirection = new Vector2(playerToHookDirection.y, -playerToHookDirection.x);
                    var rightPerpPos = (Vector2)transform.position + perpendicularDirection * 2f;
                    Debug.DrawLine(transform.position, rightPerpPos, Color.green, 0f);
                }

                var force = perpendicularDirection * swingForce;
                rBody.AddForce(force, ForceMode2D.Force);
            }
            else
            {
                //animator.SetBool("IsSwinging", false);
                if (m_Grounded)
                {
                    var groundForce = speed * 2f;
                    rBody.AddForce(new Vector2((horizontalMove * groundForce - rBody.velocity.x) * groundForce, 0));
                    rBody.velocity = new Vector2(rBody.velocity.x, rBody.velocity.y);
                }
            }

            if (!isSwinging)
            {
                //if (!groundCheck) return;

            } 
        }

    }

}
