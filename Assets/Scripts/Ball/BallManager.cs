﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour
{
    public float speed;
    //GameObject target;

    void Start()
    {
        GetComponent<Rigidbody2D> ().velocity = Vector2.down *3;
        //target = GameObject.FindGameObjectWithTag("Ground");
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Ground")
        {
            Debug.Log("Bola acertou o chao");
            Destroy(this.gameObject);
            GameManagement.destroyedBall = true;
        }
        
        FindObjectOfType<AudioController>().ballBouncing.Play();

    } 

}
